# Base image: https://hub.docker.com/_/golang/
FROM debian:bookworm
MAINTAINER Vesa-Pekka Palmu <vpalmu@depili.fi>

RUN apt-get update && apt-get install -y --no-install-recommends \
    git \
    genimage \
    dosfstools \
    mtools \
    gzip \
    openssh-client \
    sshpass \
    wget \
    zip \
    ca-certificates \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
