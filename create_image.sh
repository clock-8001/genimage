#!/bin/bash

set -e

GENIMAGE_TMP="tmp"
GENIMAGE_INPUT="images"
GENIMAGE_OUTPUT="out"
GENIMAGE_CFG="genimage-raspberrypi3.cfg"
GENIMAGE_ROOT="root"

rm -rf "${GENIMAGE_TMP}"

mkdir -p "${GENIMAGE_TMP}"
mkdir -p "${GENIMAGE_OUTPUT}"
mkdir -p "${GENIMAGE_ROOT}"

rm -fr clock-8001

git clone https://gitlab.com/clock-8001/clock-8001.git

cp -r clock-8001/v4/voices ${GENIMAGE_INPUT}

wget https://gitlab.com/clock-8001/clock-8001/-/jobs/artifacts/master/raw/v4/sdl-clock?job=build_v4 -O ${GENIMAGE_INPUT}/sdl-clock
wget https://gitlab.com/clock-8001/clock-8001/-/jobs/artifacts/master/raw/v4/clock_port80.ini?job=create_config_4 -O ${GENIMAGE_INPUT}/clock.ini


genimage                              \
    --rootpath "${GENIMAGE_ROOT}"     \
	--tmppath "${GENIMAGE_TMP}"       \
	--inputpath "${GENIMAGE_INPUT}"   \
	--outputpath "${GENIMAGE_OUTPUT}" \
	--config "${GENIMAGE_CFG}"        \
	--mkdosfs "/sbin/mkdosfs"
exit $?
